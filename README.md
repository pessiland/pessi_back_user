# pessi_back_user
### Getting started

 build the .jar by using: 

    `./mvnw clean install -DskipTests`

 then build the docker image of the service :

    `docker build -t userapp .`

 finally start the compose and wait for all the pods to be healthy:

    `docker compose up -d`

mysql port : 3307

app port : 8080

