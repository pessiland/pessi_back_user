package pessiland.pessi_back.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idUser")
    private int idUser;

    @Column(name = "Name", length = 45)
    private String name;

    public User(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }
    @JsonProperty("idUser")
    public int getIdUser() {
        return idUser;
    }

    public User() {
        // default constructor body
    }
}
