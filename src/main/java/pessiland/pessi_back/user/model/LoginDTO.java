package pessiland.pessi_back.user.model;

import java.util.*;

public class LoginDTO {
    private boolean success;
    private Integer userId;
    private String name;

    // Constructors, getters, and setters

    public LoginDTO(boolean success, Integer userId, String name) {
        this.success = success;
        this.userId = userId;
        this.name = name;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
