package pessiland.pessi_back.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pessiland.pessi_back.user.model.User;
import java.util.*;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findUserByIdUser(int idUser);
    User save(User user);
}

