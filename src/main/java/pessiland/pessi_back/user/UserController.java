package pessiland.pessi_back.user;

import pessiland.pessi_back.user.model.*;
import pessiland.pessi_back.user.service.UserService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable int id) {
        return userService.getUserById(id);
    }

    @PostMapping("/create")
    public Integer signUp(@RequestBody CreateUserDTO newUser) {
        return userService.createUser(newUser.getName());
    }

    @PostMapping("/login")
    public LoginDTO login(@RequestBody LoginRequest loginRequest) {
        return userService.loginUser(loginRequest.getUserId());
    }
}

