package pessiland.pessi_back.user.service;

import pessiland.pessi_back.user.model.LoginDTO;
import pessiland.pessi_back.user.repository.UserRepository;
import pessiland.pessi_back.user.model.User;
import org.springframework.stereotype.Service;

import java.util.*;
@Service
public class UserService {
    private final UserRepository userRepository;


    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserById(int id) {
        Optional<User> userOptional = userRepository.findUserByIdUser(id);
        return userOptional.orElse(null);
    }

    public int createUser(String name) {
        User user = new User(name);
        User createdUser = userRepository.save(user);
        return createdUser.getIdUser();
    }

    public LoginDTO loginUser(int userId) {
            // Perform authentication logic (e.g., check if the user with the provided ID exists)
            User user = this.getUserById(userId);
            if (user != null) {
                System.out.println("login successful");
                return new LoginDTO(true, userId , user.getName());
            } else {
                System.out.printf("login failed for id %d", userId);
                return new LoginDTO(false, -1, "");
            }
    }
}
