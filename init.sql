-- Create database if not exists
CREATE DATABASE IF NOT EXISTS userDB;

-- Use the created database
USE userDB;

-- Create the User table
CREATE TABLE IF NOT EXISTS User (
                                    idUser INT AUTO_INCREMENT PRIMARY KEY,
                                    Name VARCHAR(45) NOT NULL
    );